import io.kotlintest.*
import io.kotlintest.matchers.instanceOf
import io.kotlintest.runner.junit5.TestCaseContext
import io.kotlintest.specs.StringSpec

class TestGraph : StringSpec() {

    override fun afterTest(description: Description, result: TestResult) {
        Node.resetEnumeration()
    }

    init {
        "Graph() should return an empty graph with source and target" {
            val g = Graph()
            g.numNodes shouldBe 2
            g.numEdges shouldBe 0
        }

        "Graph.populate should add n nodes to the graph" {
            val g = Graph()
            g.populate(10)
            g.numNodes shouldBe 12
        }

        "Graph.numNodes should be equal to number of graph nodes" {
            val g = Graph()
            g.numNodes shouldBe 2
            g.populate(100)
            g.numNodes shouldBe 102
        }

        "graph[i] should return ith node" {
            val g = Graph()
            g[0] shouldBe instanceOf(Node()::class)
            g[2] shouldBe null
            g.populate(10)
            g[3] shouldBe instanceOf(Node()::class)
            g[12] shouldBe null
        }

        "graph[i, j] should return Edge between two nodes" {
            val g = Graph()
            g[0,1] shouldBe null
            g[0,1] = Edge()
            g[0,1] shouldNotBe null
            g[0,1]?.source shouldBe g[0]
            g[0,1]?.target shouldBe g[1]
        }

        "Graph.addRandomEdges should edges to the graph, making it a graph of requested density" {
            val g = Graph()
            g.populate(10)
            g.addRandomEdges(0.2)
            g.numEdges shouldBe 23
            g.addRandomEdges(1.0)
            g.numEdges shouldBe 111
        }

        "Graph.bfs should return path." {
            val g = Graph()
            g.populate(5)
            g.bfs() shouldBe null
            g.addRandomEdges(1.0)
            g.bfs() shouldNotBe null
        }

        "Graph.fill should return maximum flow" {
            val g = Graph()
            g.populate(5)
            g.fill(.1) shouldBe false
            g.addRandomEdges(1.0)
            g.fill(1e-90) shouldBe true
        }

        "Test blocking flow" {
            val g = Graph()
            g.populate(6)
            g[0,1] = Edge(.0, 3.0)
            g[0,3] = Edge(.0, 2.0)
            g[1,2] = Edge(.0, 2.0)
            g[1,5] = Edge(.0, 5.0)
            g[2,7] = Edge(.0, 3.0)
            g[3,4] = Edge(.0, 2.0)
            g[4,2] = Edge(.0, 2.0)
            g[5,6] = Edge(.0, 2.0)
            g[6,7] = Edge(.0, 2.0)
            g.fill(5.0) shouldBe true
        }

        "Test some shit" {
            val g = Graph()
            g.populate(10)
            g[0, 1] = Edge(.0, 1.0, 1.0)
            // Тест двойных дуг
            g[1, 0] = Edge(.0, 1.0, 1.0)
            g[1, 2] = Edge(.0, 1.0, 2.0)
            g[1, 4] = Edge(.0, 1.0, 12.0)
            g[2, 3] = Edge(.0, 1.0, 1.0)
            g[2, 10] = Edge(.0, 1.0, 1.0)
            g[3, 4] = Edge(.0, 1.0, 3.0)
            g[3, 2] = Edge(.0, 1.0, 1.0)
            g[4, 5] = Edge(.0, 1.0, 5.0)
            g[5, 6] = Edge(.0, 1.0, 1.0)
//             Тест двойных дуг
            g[7, 6] = Edge(.0, 1.0, 1.0)
            g[6, 7] = Edge(.0, 1.0, 2.0)
            g[6, 9] = Edge(.0, 1.0, 12.0)
            g[7, 8] = Edge(.0, 1.0, 1.0)
            g[8, 9] = Edge(.0, 1.0, 3.0)
            g[9, 11] = Edge(.0, 1.0, 5.0)
            g.fill(1.0) shouldBe true
//            println(g)
            g.optimizeFlow()
            println(g)
        }
    }
}