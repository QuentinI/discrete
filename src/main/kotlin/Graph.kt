import java.lang.Double.min
import java.util.*

// Класс вершины
class Node(val id: String = "${++nid}") {
    val iedges: MutableMap<Node, Edge> = mutableMapOf() // Словарь входящих рёбер (пары вершина-ребро)
    val oedges: MutableMap<Node, Edge> = mutableMapOf() // Словарь рёбер (пары вершина-ребро)

    // Ребра остаточной сети, генерируются динамически
    val viedges: Map<Node, Edge>
        get() = oedges.mapValues { it.value.copy(source = it.value.target, target = it.value.source,  weight = -it.value.weight, flow = -it.value.flow) }
    val voedges: Map<Node, Edge>
        get() = iedges.mapValues { it.value.copy(source = it.value.target, target = it.value.source,  weight = -it.value.weight, flow = -it.value.flow) }

    override fun toString(): String {
        return id
    }

    // Найти ребро до другой вершины
    fun edgeTo(other: Node?): Edge? {
        return oedges[other]
    }

    // Найти ребро от другой вершины
    fun edgeFrom(other: Node): Edge? {
        return iedges[other]
    }

    companion object {
        var nid: Int = 0
        fun resetEnumeration() {
            nid = 0
        }
    }
}

// Класс пути (список ребер)
data class Path(val edges: MutableList<Edge> = mutableListOf()) {
    // Пропускная способность пути (определяется ребром с минимальной пропускной способностью)
    val capacity: Double
        get() = edges.fold(Double.MAX_VALUE, {acc, edge -> min(acc, edge.residualCapacity)})
    // Длина пути
    val size: Int
        get() = edges.size

    // Доступ к энному ребру пути (path[n])
    operator fun get(i: Int): Edge {
        return edges[i]
    }

    // Пустить заданный поток по пути
    fun pushFlow(f: Double) {
        // Если поток больше максимально возможного, пускаем максимально возможный
        val flowToPush = min(f, capacity)
        for (edge in edges) {
            edge.flow += flowToPush
        }
    }

    // Добавить ребро в конец пути
    fun add(e: Edge) {
        edges.add(e)
    }

    // Добавить ребро в начало пути
    fun push(e: Edge) {
        edges.add(0, e)
    }

    // Проверить, есть ли ребро в пути (edge in path)
    operator fun contains(e: Edge): Boolean {
        return e in edges
    }

    // Срез пути от одного ребра до другого
    fun slice(fromIndex: Int, toIndex: Int): Path {
        return Path(edges.subList(fromIndex, toIndex))
    }

    // Номер ребра в пути
    fun indexOf(e: Edge): Int {
        return edges.indexOf(e)
    }

    // Итератор (for (edge in path) {...})
    operator fun iterator(): MutableIterator<Edge> {
        return edges.iterator()
    }

}

// Класс ребра
data class Edge(var flow: Double = .0,                  // Величина потока, по умолчанию ноль
                var capacity: Double = 1.0,             // Максимальный поток, по умолчанию единица
                var weight: Double = 1.0,               // Вес ребра
                var target: Node = Node("dummy"),    // Из какой вершины
                var source: Node = Node("dummy"))  { // В какую
    // Доступная пропускная способность (сколько еще потока можно пустить через это ребро)
    val residualCapacity: Double
        get() = capacity - flow

    override fun toString(): String {
        return "Ребро (вес $weight, поток $flow/$capacity) // из $source в $target"
    }
}

// Класс графа
class Graph {
    // Сразу при создании заводим две вершины
    // Источник
    private val source: Node = Node("0")
    // И сток
    private val target: Node = Node("∞")
    private var nodes: MutableList<Node> = mutableListOf(source, target) // Список вершин
    private var edges: MutableList<Edge> = mutableListOf()               // Список рёбер
    // Количество вершин
    val numNodes: Int
        get() = nodes.size
    // Количество рёбер
    val numEdges: Int
        get() = edges.size

    /**
     * Функция для добавления в граф заданного количества вершин
     * @param n - желаемое количество вершин
     */
    fun populate(n: Int) {
        nodes.remove(target)                       // Забираем сток из списка, чтобы он всегда был в конце
        (1..n).forEach { _ -> nodes.add(Node()) }  // Добавляем вершины
        nodes.add(target)                          // Возвращаем в конец списка
    }

    // Функция для доступа к вершине по номеру
    // Работает так: graph[i]
    operator fun get(i: Int) : Node? {
        return nodes.getOrNull(i)
    }

    // Функция для доступа к ребру по номерам вершин
    // graph[i, j] -> ребро из i в j (или null, если такого ребра нет)
    operator fun get(i: Int, j: Int) : Edge? {
        val ith = nodes.getOrNull(i)
        val jth = nodes.getOrNull(j)
        return ith?.edgeTo(jth)
    }

    // Функция для добавления рёбер
    // graph[i, j] = Edge(...)
    operator fun set(i: Int, j: Int, e: Edge) {
        // Входящая и исходящая вершина внутри ребра проставляются автоматически
        e.source = nodes[i]
        e.target = nodes[j]
        edges.add(e)
        // Добавляем в список исходящих ребер первой вершины
        nodes[i].oedges[nodes[j]] = e
        // И список входящих второй
        nodes[j].iedges[nodes[i]] = e
    }

    /**
     * Функция для увеличения связности графа
     * @param density - желаемая связность
     */
    fun addRandomEdges(density: Double) {
        // Связность не может быть больше единицы
        assert(density <= 1.0)
        // Считаем, сколько рёбер нам нужно добавить
        var x : Int = ((density * (numNodes - 1) * (numNodes - 2)) + 1 - numEdges).toInt()
        // Если их добавлять не нужно, то и не будем
        if (x < 1) return
        // Если у нас нет ни одного ребра из источника, надо его добавить
        if (source.oedges.isEmpty()) {
            val tgt = Random().nextInt(numNodes - 1) + 1
            set(0, tgt, Edge())
            x -= 1
        }

        if (x < 1) return
        // Если у нас нет ни одного ребра в сток, надо его добавить
        if (target.iedges.isEmpty()) {
            val src = Random().nextInt(numNodes - 2)
            set(src, numNodes - 1, Edge())
            x -= 1
        }

        // Создаем рандомные ребра
        for (i in 1..x) {
            val r = Random() // Генератор рандомных чисел
            var src = 0
            var tgt = 0
            // Перебрасываем кубик, если
            while (
               src == tgt || // Вершины совпадают
               get(src, tgt) != null || // Вершины уже соединены
               r.nextInt(nodes[src].oedges.size + 1) != 0 || // Стараемся сделать граф равномерно связным, уменьшая вероятность
               r.nextInt(nodes[tgt].iedges.size + 1) != 0    // Появления каждого следующего исходящего/входящего ребра у одной вершины
            ) {
                src = r.nextInt(numNodes - 1)
                tgt = r.nextInt(numNodes - 1) + 1
            }
            // Соединяем ребром
            val randCapacity = r.nextDouble()
            val randWeight = r.nextDouble()
            set(src, tgt, Edge(.0, randCapacity, randWeight))
        }
    }

    /**
     * Поиск в ширину
     * @param src - начало пути, по умолчанию источник
     * @param tgt - конец пути, по умолчанию сток
     */
    fun bfs(src: Node = source, tgt: Node = target): Path? {
        // Список вершин, в которых нужно побывать
        val queue: Queue<Node> = LinkedList<Node>()
        // Словарь, хранящий статус вершины (поесещена/не посещена)
        val visited: MutableMap<Node, Boolean> = mutableMapOf()
        // Словарь, хранящий для каждой вершины ее родителя (нужен, чтобы восстановить путь)
        val parents: MutableMap<Node, Edge> = mutableMapOf()
        // Изначально все вершины не посещены
        for (n in nodes) visited[n] = false
        // Текущая вершина
        var node: Node
        // Отмечаем начало пути как посещенную...
        visited[src] = true
        // И добавляем в очередь.
        queue.add(src)
        // Пока в очереди есть вершины...
        while (queue.size != 0) {
            // Забираем оттуда первую
            node = queue.poll()
            // Если это цель пути, восстанавливаем путь с помощью словаря родителей
            if (node == tgt) {
                val path = Path()
                var cursor: Node = tgt
                while (cursor != src) {
                    path.push(parents[cursor]!!)
                    cursor = parents[cursor]!!.source
                }
                return path
            }

            val traverseNode = { child:Node, edge: Edge ->
                if ( !visited[child]!! && edge.residualCapacity > .0 ) {
                    // Добавляем эту вершину в очередь на посещение
                    queue.add(child)
                    // Отмечаем как посещенную
                    visited[child] = true
                    // И указываем, откуда мы в нее пришли
                    parents[child] = edge
                }
            }

            // Если это не цель пути, перебираем исходящие ребра
            for ((child, edge) in node.oedges) {
                traverseNode(child, edge)
            }
            for ((child, edge) in node.voedges) {
                traverseNode(child, edge)
            }
        }
        // Не нашли путь
        return null
    }

    /**
     * Модифицированный алгоритм Эдмондса-Карпа. Не ищет максмальный поток, а пускает заданный поток по сети
     * @param desiredFlow - заданный поток
     * @return true, если поток не превысил максимальный возможный и false в обратном случае
     */
    fun fill(desiredFlow: Double): Boolean {
        // Величина потока, для которой еще нужно найти место
        var remainingFlow = desiredFlow
        // Ищем свободный путь из источника в сток
        var path = bfs()
        // Пока такой путь есть, а оставшийся поток не стал пренебрежимо мал
        while(path != null && remainingFlow >= 1e-100) {
            val pushableFlow = min(path.capacity, remainingFlow)
            // Увеличиваем поток по всему пути на максимально возможный (или на оставшийся, если он влезает целиком)
            path.pushFlow(pushableFlow)
            // Уменьшаем величину оставшегося потока
            remainingFlow -= pushableFlow
            // Ищем новый свободный путь
            path = bfs()
        }
        // Удалось ли вместить заданный поток?
        return remainingFlow <= 1e-100
    }

    // Генерация ребер остаточной сети
    private fun generateVirtualEdges(): MutableList<Edge> {
        val virtualEdges: MutableList<Edge> = mutableListOf()
        // Формируем список ребер остаточной сети
        for (edge in edges) {
            // Добавляем антисимметричное ребро
            virtualEdges.add(edge.copy(source = edge.target, target = edge.source,  weight = -edge.weight, flow = -edge.flow))
            if (edge.residualCapacity > .0) {
                // Если по ребру еще можно пустить поток, добавляем его
                virtualEdges.add(edge.copy())
            } else {
                // Иначе добавляем его с максимальной стоимостью
                virtualEdges.add(edge.copy(weight = Double.MAX_VALUE))
            }
        }

        return virtualEdges
    }

    /**
     * Алгоритм Беллмана-Форда для поиска "отрицательных циклов", по которым
     * мы можем пустить поток в обратную сторону, минимизировав стоимость.
    */
    fun optimizeFlow() {
        // Список родителей - для поиска циклов
        var parents: MutableMap<Node, Pair<Node, Edge>>
        // Настоящий список ребер нам не подходит, потому что алгоритм Б-Ф требует антисимметрии для каждого ребра
        var virtualEdges: MutableList<Edge>
        // Словарь расстояний от стока графа
        var distances: MutableMap<Node, Double>
        // Ожидаем отрицательный цикл
        var haveToSearch = true
        // Ищем до первой итерации, на которой не нашли отрицательный цикл
        while (haveToSearch) {
            // Сбрасываем флаг поиска отрицательного цикла (поднимем, если найдем еще один)
            haveToSearch = false

            // Обнуляем списки родителей и расстояний
            parents = mutableMapOf()
            distances = mutableMapOf()
            // Генерируем новый список фиктивных ребер
            virtualEdges = generateVirtualEdges()
            // Инициализируем расстояния до всех вершин запредельно большим числом
            nodes.forEach { distances[it] = Double.MAX_VALUE }
            // От стока до стока расстояние равно нулю, очевидно
            distances[target] = .0

            for (i in 1..(virtualEdges.size)) {
                // Обходим все рёбра
                for (edge in virtualEdges) {
                    // Если через это ребро расстояние до его конца получается меньше, уменьшаем его в словаре (релаксация)
                    if (distances[edge.source] != Double.MAX_VALUE && distances[edge.source]!! + edge.weight < distances[edge.target]!!) {
                        distances[edge.target] = distances[edge.source]!! + edge.weight
                        // И добавляем в список родителей, но не допускаем ситуации, когда две вершины являются родителями друг друга
                        // (это встречные дуги, которые дают ложный отрицательный цикл)
                        if (parents[edge.source]?.first != edge.target ) {
                            parents[edge.target] = Pair(edge.source, edge)
                        }
                    }
                }
            }

            // Обходим все ребра еще раз
            iter@for (edge in virtualEdges) {
                // Если мы опять можем уменьшить расстояние от стока до этой вершины, значит, в графе есть отрицательный цикл
                if (distances[edge.source]!! + edge.weight < distances[edge.target]!!) {

                    // Цикл
                    var cycle = Path()
                    var nxt: Edge = edge
                    // Обходим ребра, пока не придем к тому, из которого начали
                    while (nxt !in cycle) {
                        // Если у какой-то вершины нет родителя, то это не цикл, а встречные дуги, идем дальше по списку ребер
                        if (parents[nxt.source] == null) continue@iter
                        // Добавляем ребро в цикл
                        cycle.add(nxt)
                        // Следующее ребро
                        nxt = parents[nxt.source]!!.second
                    }
                    // На этой итерации нашли цикл, поднимаем флаг
                    haveToSearch = true

                    // Вырезаем из из цикла "хвост" (оставляем только часть, которая действительно замыкается)
                    cycle = cycle.slice(cycle.indexOf(nxt), cycle.size)
                    // Пропускная способность цикла
                    val minVal = cycle.capacity
                    // Обходим весь цикл и инвертируем поток в настоящих ребрах, если они есть
                    for (e in cycle) {
                        val realEdge = e.source.edgeTo(e.target)
                        val realBackwardEdge = e.target.edgeTo(e.source)
                        if (realEdge != null) realEdge.flow += minVal
                        if (realBackwardEdge != null) realBackwardEdge.flow -= minVal
                    }

                    break@iter

                }
            }
        }

        return
    }

    override fun toString(): String {
        var ret = "Сеть(источник=$source, сток=$target)"
        for (n in nodes) {
            ret = "$ret\nВершина $n"
            for ((node, edge) in n.oedges) {
                ret = "$ret\n\t-> $node $edge"
            }
        }
        return ret
    }
}